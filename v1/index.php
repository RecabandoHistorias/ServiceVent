﻿<?php
//error_reporting(E_ALL);

require '.././libs/Slim/Slim.php';
require '../include/db.php';
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim(); 


	$app->get('/api/ventasdetail', function() use ($app) {
		 $apikey = $app->request->headers->get('apikey');
		
		 $consulta = "select codeProd,description,creationDate,metodoPago,estadoPago,direccionEnvio,estadoEnvio,prePago,MontoTotal,transportCompan,trackNumber,fechaentrega,fechahora,
					 c.contactName,c.contactDni,c.contactphone,c.contactMail from productopedido ped inner JOIN
					 contacto c where c.idContacto = ped.idContacto";
		
		 
		$validate = ValidateApikey($apikey);
		 if($validate == true){
			
			 try{
			// Instanciar la base de datos
			 $db = new db();
			 $response = array();
			// Conexión
			 $db = $db->conectar();
			 $ejecutar = $db->query($consulta);
			 // $stmt = $db->query("SELECT * FROM productopedido");
			 $ventas = $ejecutar->fetchAll(PDO::FETCH_OBJ);
			
			 $object = new stdClass();
			 $object->data = array();
			 foreach ($ventas as $key => $value)
			 {
				 array_push($object->data, $value);
			 }
				 $db = null;  
				 echoRespnse(200, $object);
				 
			 // $array = array ("contacto"=> $clientes );
			  // echoRespnse(200, $array);

			} catch(PDOException $e){
				 $response["error"] = true;
				 $response["message"] ="Hubo un error al listar";
				 echoRespnse(404, $response);
			}
			   
			   
		 }else{
			 
			 $response["error"] = true;
			 $response["message"] = 'apikey invalido';
			 echoRespnse(404, $response);
		
		 }
	});


	$app->post('/api/ventas/agregar',  function() use ($app) {
		
		 $apikey = $app->request->headers->get('apikey');
	     $json = $app->request->getBody();
	     $data = json_decode($json, true);
		 
		 $validate = ValidateApikey($apikey);
		 if($validate == true){
	
			 $contactName = $data['contactName'];
			 $contactDni = $data['contactDni'];
			 $contactphone = $data['contactphone'];
			 $contactMail = $data['contactMail'];
			 $adicional = $data['adicional'];
			 $codeProd = $data['codeProd'];
			 $description = $data['description'];
			 $creationDate = $data['creationDate'];
			 $metodoPago = $data['metodoPago'];
			 $estadoPago =$data['estadoPago'];
			 $direccionEnvio = $data['direccionEnvio'];
			 $estadoEnvio = $data['estadoEnvio'];
			 $prePago = $data['prePago'];
			 $MontoTotal = $data['MontoTotal'];
			 $transportCompan = $data['transportCompan'];
			 $trackNumber = $data['trackNumber'];
			 $fechaentrega = $data['fechaentrega'];
			 $fechahora = $data['fechahora'];
		
			 $response = array();
			 $consultacontact = "INSERT INTO contacto(contactName,contactDni,contactphone,contactMail,adicional)values(:contactName, :contactDni, :contactphone,:contactMail,:adicional)";
		 
		   
			try{
				// Instanciar la base de datos
				$db = new db();
				// Conexión
				$db = $db->conectar();
				$stmtc = $db->prepare($consultacontact);
				$stmtc->bindParam(':contactName', $contactName);
				$stmtc->bindParam(':contactDni',  $contactDni);
				$stmtc->bindParam(':contactphone',  $contactphone);
				$stmtc->bindParam(':contactMail',    $contactMail);
				$stmtc->bindParam(':adicional',    $Adicional);
				$stmtc->execute();
				$idlast= $db->lastInsertId();
				
				 $consultaprod = "INSERT INTO productopedido (codeProd, description, creationDate, metodoPago, estadoPago, direccionEnvio, estadoEnvio, prePago, MontoTotal, transportCompan,trackNumber,fechaentrega,fechahora,idContacto) VALUES
					(:codeProd, :description, :creationDate, :metodoPago, :estadoPago, :direccionEnvio, :estadoEnvio, :prePago, :MontoTotal, :transportCompan,:trackNumber,:fechaentrega,:fechahora, ".$idlast.")";
				   
						
				 $stmt = $db->prepare($consultaprod);
				 $stmt->bindParam(':codeProd', $codeProd);
				 $stmt->bindParam(':description',  $description);
				 $stmt->bindParam(':creationDate',  $creationDate);
				 $stmt->bindParam(':metodoPago',    $metodoPago);
				 $stmt->bindParam(':estadoPago',  $estadoPago);
				 $stmt->bindParam(':direccionEnvio',   $direccionEnvio);
				 $stmt->bindParam(':estadoEnvio',   $estadoEnvio);
				 $stmt->bindParam(':prePago', $prePago);
				 $stmt->bindParam(':MontoTotal',  $MontoTotal);
				 $stmt->bindParam(':transportCompan',   $transportCompan);
				 $stmt->bindParam(':trackNumber',    $trackNumber);
				 $stmt->bindParam(':fechaentrega',   $fechaentrega);
				 $stmt->bindParam(':fechahora',   $fechahora);
				 // $array = array ("idlast"=> $clientes );
				  // echoRespnse(200, $array);
				
				 $stmt->execute();
				 
				  $response["estado"] = 1;
				  $response["resultado"] = "ok";
				  $response["message"] = "Pedido registrado con exito";
				  echoRespnse(200, $response);
				  
			} catch(PDOException $e){
				 $response["error"] = true;
				// $detail = $db->errorInfo();
				 $response["message"] ="Hubo un error al registrar";
				 
				 echoRespnse(404, $response);
			}
			
		 }else{
			 
			 $response["error"] = true;
			 $response["message"] = 'Apikey invalido';
			 echoRespnse(404, $response);
		
		 }
	 });



	 $app->put('/api/ventas/editpedido/:cargas_id', function($cargas_id) use($app) {
		 $apikey = $app->request->headers->get('apikey');
		 
		// $estado = $app->request->put('estado');
		 $json = $app->request->getBody();
	     $data = json_decode($json, true);
		 
		  $validate = ValidateApikey($apikey);
			 if($validate == true){
			 
				 if($cargas_id){
					 
					 
					 $contactName = $data['contactName'];
					 $contactDni = $data['contactDni'];
					 $contactphone = $data['contactphone'];
					 $contactMail = $data['contactMail'];
					 $adicional = $data['adicional'];
					 $description = $data['description'];
					 $creationDate = $data['creationDate'];
					 $metodoPago = $data['metodoPago'];
					 $estadoPago =$data['estadoPago'];
					 $direccionEnvio = $data['direccionEnvio'];
					 $estadoEnvio = $data['estadoEnvio'];
					 $prePago = $data['prePago'];
					 $MontoTotal = $data['MontoTotal'];
					 $transportCompan = $data['transportCompan'];
					 $trackNumber = $data['trackNumber'];
					 $fechaentrega = $data['fechaentrega'];
					 $fechahora = $data['fechahora'];
					 $adicional = $data['adicional'];
					 
					 $consultaedit ="UPDATE productopedido as p INNER JOIN contacto as cn on p.idContacto = cn.idContacto
									 set p.description = ?,p.creationDate= ?,p.metodoPago = ? , p.estadoPago= ?,p.direccionEnvio= ?,p.estadoEnvio= ?,p.prePago = ?,
									 p.MontoTotal= ?,p.transportCompan = ?, p.trackNumber = ?, p.fechaentrega= ?, p.fechahora = ?, cn.contactName = ?, 
									 cn.contactDni = ?,cn.contactMail = ?, cn.contactphone = ?, cn.Adicional = ? where p.idProd = ". $cargas_id." ";
						 
					 try{
						 
						 $db = new db();
						 $db = $db->conectar();
						 $response = array();
						 $stmt = $db->prepare($consultaedit);
				
						 $stmt->bindParam(1, $description);
						 $stmt->bindParam(2, $creationDate);
						 $stmt->bindParam(3, $metodoPago);
						 $stmt->bindParam(4, $estadoPago);
						 $stmt->bindParam(5, $direccionEnvio);
						 $stmt->bindParam(6, $estadoEnvio);
						 $stmt->bindParam(7, $prePago);
						 $stmt->bindParam(8, $MontoTotal);
						 $stmt->bindParam(9, $transportCompan);
						 $stmt->bindParam(10, $trackNumber);
						 $stmt->bindParam(11, $fechaentrega);
						 $stmt->bindParam(12, $fechahora);
						 $stmt->bindParam(13, $contactName);
						 $stmt->bindParam(14, $contactDni);
						 $stmt->bindParam(15, $contactMail);
						 $stmt->bindParam(16, $contactphone);
						 $stmt->bindParam(17, $adicional);
						 $stmt->execute();
						 $result = $stmt->rowCount();

						if ($result) {
							 $response["estado"] = 1;
							 $response["resultado"] = "ok";
							 $response["message"] = "Pedido actualizado exitosamente";
							 echoRespnse(200, $response);
							 
						}else{
									 
							 $response["estado"] = 0;
							 $response["message"] = "No se hicieron mas cambios";
							 echoRespnse(404, $response);
						}	
					
					 }catch(PDOException $e){
					   
						 $response["error"] = true;
						 $response["message"] ="Hubo un error al actualizar el pedido";
						  echoRespnse(404, $response);
					 }
					
				 }else{
					 
						 $response["error"] = true;
						 $response["message"] ="No ha especificado el id del pedido";
						 echoRespnse(404, $response);
					 
				 }
		 }else{
			 
			 $response["error"] = true;
			 $response["message"] = 'apikey invalido';
			 echoRespnse(404, $response);
		
		 }
           
     });
	
	 
	 $app->delete('/api/ventas/elimipedido/:cargas_id', function($cargas_id) use($app) {
			 $apikey = $app->request->headers->get('apikey');
			 
			  $validate = ValidateApikey($apikey);
			  if($validate == true){
			 
				 if($cargas_id){
					 $consultadelet ="DELETE from productopedido where idProd = ". $cargas_id." ";
				 
					 try{
						 
						 $db = new db();
						 $response = array();
						 // Conexión
						 $db = $db->conectar();
									
						 $stmt = $db->prepare($consultadelet);
						
						 $stmt->execute();
						 $result = $stmt->rowCount();
						 
						 if($result){
							 
						  $response["estado"] = 1;
						  $response["resultado"] = "ok";
						  $response["message"] = "Pedido eliminado con exito";
						  echoRespnse(404, $response);
						  
						 }else{
							 
							 $response["estado"] = 0;
							 $response["message"] = "Id Pedido no existente";
							 echoRespnse(404, $response);
						 }
						 
					  } catch(PDOException $e){
						 $response["error"] = true;
						 $response["message"] ="Hubo un error al eliminar el pedido";
						 
						 echoRespnse(404, $response);
					 }
				 
				 }else{
					 
					 $response["error"] = true;
					 $response["message"] ="No ha especificado el id del pedido";
					 
					 echoRespnse(404, $response);
				 }
			 }else{
			 
				 $response["error"] = true;
				 $response["message"] = 'apikey invalido';
				 echoRespnse(404, $response);
		
			 }
		 
	 });
	 
	  function ValidateApikey($apikey){
		     $db = new db();
			 $db = $db->conectar();
			 $consultadelet ="Select * from seguridad where apikey = :apikey ";
			 $stmt = $db->prepare($consultadelet);
			  $stmt->bindparam(':apikey', $apikey);
			 // $ejecutar = $db->query($consultadelet);
			
			  $execute = $stmt->execute();
			 

			 if( $stmt->rowCount() > 0){
			
				 return true;
			 }else{
				
				 return false;
			 }
			 
	  }


	  function echoRespnse($status_code, $response) {
          $app = \Slim\Slim::getInstance();
          $app->status($status_code);
          $app->contentType('application/json');

			echo json_encode($response);
     }
	 
	 $app->run();
?>