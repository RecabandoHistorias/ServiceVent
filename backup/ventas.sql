/*
Navicat MySQL Data Transfer

Source Server         : Mysql
Source Server Version : 50625
Source Host           : localhost:3306
Source Database       : ventas

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2018-07-15 19:11:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for contacto
-- ----------------------------
DROP TABLE IF EXISTS `contacto`;
CREATE TABLE `contacto` (
  `idContacto` int(11) NOT NULL AUTO_INCREMENT,
  `contactName` varchar(50) NOT NULL,
  `contactDni` varchar(8) NOT NULL,
  `contactphone` varchar(12) NOT NULL,
  `contactMail` char(15) NOT NULL,
  `Adicional` varchar(25) NOT NULL,
  PRIMARY KEY (`idContacto`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for productopedido
-- ----------------------------
DROP TABLE IF EXISTS `productopedido`;
CREATE TABLE `productopedido` (
  `idProd` int(11) NOT NULL AUTO_INCREMENT,
  `codeProd` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `creationDate` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `metodoPago` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `estadoPago` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `direccionEnvio` varchar(75) CHARACTER SET utf8 DEFAULT NULL,
  `estadoEnvio` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `prePago` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `MontoTotal` int(11) DEFAULT NULL,
  `transportCompan` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `trackNumber` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `fechaentrega` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `fechahora` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `contactName` varchar(50) NOT NULL,
  `contactDni` varchar(8) NOT NULL,
  `contactphone` varchar(11) DEFAULT NULL,
  `contactMail` char(20) DEFAULT NULL,
  `idContacto` int(11) NOT NULL,
  PRIMARY KEY (`idProd`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for seguridad
-- ----------------------------
DROP TABLE IF EXISTS `seguridad`;
CREATE TABLE `seguridad` (
  `idApikey` int(8) NOT NULL AUTO_INCREMENT,
  `apikey` char(30) NOT NULL,
  `estado` int(1) DEFAULT NULL,
  PRIMARY KEY (`idApikey`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
